<?php
/**
 * @author Fabian Fülling <fabian.fuelling@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de
 * @created 08.04.14 17:35
 */

namespace OC\Util;

class ImplodeRecursiveTest extends \PHPUnit_Framework_TestCase
{
    public function testImplodeRecursive()
    {
        $expected = 'a: 1, 2, 3, b: 1, 2, 3, c: 1, 2, 3';

        $array = array(
            'a' => array(1, 2, 3),
            'b' => array(1, 2, 3),
            'c' => array(1, 2, 3),
        );

        $this->assertSame($expected, ArrayHelper::implodeRecursive($array));
    }
}
