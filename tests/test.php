<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 29.12.13
 */

require dirname(__DIR__) . '/vendor/autoload.php';

$array1 = array(
    'a' => array(1, 2, 3),
    'b' => array(1, 2, 3),
    'c' => array(1, 2, 3),
);

echo \OC\Util\ArrayHelper::implodeRecursive($array1);

$array2 = [
    'lorem' => 'ipsum',
    'pets' => [
        'fish' => [
            'orca' => 'willy',
            'clown' => 'nemo',
        ],
        'cat' => 'Garfield',
        'dog' => 'Lassie',
    ],
    'children' => [
        ['name' => 'tom', 'age' => 7],
        ['name' => 'jerry', 'age' => 8],
    ]
];

print_r(\OC\Util\ArrayHelper::toDotNotation($array2, '.'));

$array3 = [
    'lorem' => 'ipsums',
    'pets' => [
        'fish' => [
            'orca' => 'willie',
            'clown' => 'nemo',
        ],
        'dogs' => 'Lassie',
    ],
    'children' => [
        ['name' => 'tim', 'age' => 7],
        ['name' => 'jerry', 'age' => 8],
        ['name' => 'ben', 'age' => 12],
    ]
];

print_r(\OC\Util\ArrayHelper::diffRecursive($array3, $array2, false));
print_r(\OC\Util\ArrayHelper::diffRecursive($array3, $array2, true));
