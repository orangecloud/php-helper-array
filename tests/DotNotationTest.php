<?php
/**
 * @author Fabian Fülling <fabian.fuelling@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de
 * @created 08.04.14 16:42
 */

namespace OC\Util;

class DotNotationTest extends \PHPUnit_Framework_TestCase
{
    protected $dotNotation = [
        'lorem' => 'ipsum',
        'pets.fish.orca' => 'willy',
        'pets.fish.clown' => 'nemo',
        'pets.cat' => 'Garfield',
        'pets.dog' => 'Lassie',
        'children.0.name' => 'tom',
        'children.0.age' => 7,
        'children.1.name' => 'jerry',
        'children.1.age' => 8,
    ];

    protected $dotNotationAssocOnly = [
        'lorem' => 'ipsum',
        'pets.fish.orca' => 'willy',
        'pets.fish.clown' => 'nemo',
        'pets.cat' => 'Garfield',
        'pets.dog' => 'Lassie',
        'children' => [
            ['name' => 'tom',   'age' => 7],
            ['name' => 'jerry', 'age' => 8]
        ]
    ];

    protected $recursive = [
        'lorem' => 'ipsum',
        'pets' => [
            'fish' => [
                'orca' => 'willy',
                'clown' => 'nemo',
            ],
            'cat' => 'Garfield',
            'dog' => 'Lassie',
        ],
        'children' => [
            ['name' => 'tom', 'age' => 7],
            ['name' => 'jerry', 'age' => 8],
        ]
    ];

    public function testToDotNotation()
    {
        $dotNotation = ArrayHelper::toDotNotation($this->recursive);
        $this->assertSame($this->dotNotation, $dotNotation);
    }

    public function testToDotNotationAssocOnly()
    {
        $dotNotation = ArrayHelper::toDotNotation($this->recursive, '.', [], [], true);
        $this->assertSame($this->dotNotationAssocOnly, $dotNotation);
    }

    public function testFromDotNotation()
    {
        $recursive = ArrayHelper::fromDotNotation($this->dotNotation, '.');
        $this->assertSame($this->recursive, $recursive);
    }
}
