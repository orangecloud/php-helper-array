<?php
/**
 * @author Fabian Fülling <fabian.fuelling@rocket-internet.de>
 * @copyright Copyright (c) 2014 Rocket Internet GmbH, Johannisstraße 20, 10117 Berlin, http://www.rocket-internet.de
 * @created 08.04.14 17:35
 */

namespace OC\Util;

class DiffRecursiveTest extends \PHPUnit_Framework_TestCase
{
    public function testDiffRecursive()
    {
        $array1 = [
            'lorem' => 'ipsum',
            'pets' => [
                'fish' => [
                    'orca' => 'willy',
                    'clown' => 'nemo',
                ],
                'cat' => 'Garfield',
                'dog' => 'Lassie',
            ],
            'children' => [
                ['name' => 'tom', 'age' => 7],
                ['name' => 'jerry', 'age' => 8],
            ]
        ];


        $array2 = [
            'lorem' => 'ipsums',
            'pets' => [
                'fish' => [
                    'orca' => 'willie',
                    'clown' => 'nemo',
                ],
                'dogs' => 'Lassie',
            ],
            'children' => [
                ['name' => 'tim', 'age' => 7],
                ['name' => 'jerry', 'age' => 8],
                ['name' => 'ben', 'age' => 12],
            ]
        ];

        $expected = [
            'lorem' => 'ipsum',
            'pets' => [
                'fish' => [
                    'orca' => 'willy',
                ],
                'cat' => 'Garfield',
                'dog' => 'Lassie',
            ],
            'children' => [
                0 => [
                    'name' => 'tom',
                ],
            ],
        ];

        $expectedWithNull = [
            'lorem' => 'ipsum',
            'pets' => [
                'fish' => [
                    'orca' => 'willy',
                ],
                'cat' => 'Garfield',
                'dog' => 'Lassie',
                'dogs' => null,
            ],
            'children' => [
                0 => [
                    'name' => 'tom',
                ],
               2 => NULL,
            ],
        ];

        $this->assertEquals($expected, ArrayHelper::diffRecursive($array1, $array2));
        $this->assertEquals($expectedWithNull, ArrayHelper::diffRecursive($array1, $array2, true));
        $this->assertEquals($array1, ArrayHelper::diffRecursive($array1, []));
    }
}
