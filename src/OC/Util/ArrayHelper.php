<?php
/**
 * @author Fabian Fülling <fabian@fabfuel.de>
 * @created 29.12.13
 */

namespace OC\Util;

class ArrayHelper
{
    /**
     * Implode a two-dimensional array with the keys of the first dimension as group names
     *
     * @param array $array
     * @param string $glueGroups
     * @param string $glueElements
     * @return string
     */
    public static function implodeRecursive(array $array, $glueGroups = ': ', $glueElements = ', ') {
        $imploded = array();
        foreach ($array as $group => $values) {
            $imploded[] .= $group . $glueGroups . implode($values, $glueElements);
        }
        return implode($imploded, $glueElements);
    }

    /**
     * Convert a multi-dimensional array into a flat array
     * with keys containing the depth in dot notation
     *
     * @param array $array Multidimensional Array
     * @param string $separator Defaults to '.'
     * @param array $result Used for recursion
     * @param array $previousKeys
     * @param bool $assocOnly if true only associative arrays would be convert to dot notation
     * @return array
     */
    public static function toDotNotation(array $array, $separator = '.', $result = array(), $previousKeys = array(), $assocOnly = false)
    {
        foreach ($array as $key => $value) {
            $keys = array_merge($previousKeys, array($key));
            if (is_array($value) && (!$assocOnly || static::isAssoc($value))) {
                $result = array_merge($result, static::toDotNotation($value, $separator, $result, $keys, $assocOnly));
            } else {
                $result[implode($separator, $keys)] = $value;
            }
        }
        return $result;
    }

    /**
     * Is the given array associative or not
     *
     * @param array $array
     * @return bool
     */
    public static function isAssoc($array)
    {
        return ($array !== array_values($array));
    }

    /**
     * Convert a flat array with dot-notation to an recursive array
     *
     * @param array $dotNotationArray Flat array with dot-notation
     * @param string $separator Default to '.'
     * @param array $result Used for recursion
     * @return array
     */
    public static function fromDotNotation(array $dotNotationArray, $separator = '.', &$result = [])
    {
        foreach ($dotNotationArray as $key => $value) {
            if (strpos($key, $separator) === false) {
                $result[$key] = $value;
            } else {
                $parts = explode($separator, $key, 2);
                static::fromDotNotation([$parts[1] => $value], $separator, $result[$parts[0]]);
            }
        }
        return $result;
    }

    /**
     * @param array $newData
     * @param array $originalData
     * @param bool $setMissingToNull
     * @return array
     */
    public static function diffRecursive(array $newData, array $originalData, $setMissingToNull = false)
    {
        if (empty($originalData)) {
            return $newData;
        }
        $modified = [];
        foreach ($newData as $field => $value) {
            if (is_array($value) && isset($originalData[$field])) {
                $diff = static::diffRecursive($value, $originalData[$field], $setMissingToNull);
                if ($diff) {
                    $modified[$field] = $diff;
                }
            } elseif (!isset($originalData[$field]) || $value !== $originalData[$field]) {
                $modified[$field] = $value;
            }
        }
        if ($setMissingToNull) {
            foreach ($originalData as $field => $value) {
                if (!isset($newData[$field])) {
                    $modified[$field] = null;
                }
            }
        }
        return $modified;
    }
}
